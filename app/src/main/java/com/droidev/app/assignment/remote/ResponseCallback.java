package com.droidev.app.assignment.remote;

import com.droidev.app.assignment.remote.response.ResponseError;

public interface ResponseCallback<T> {

    void onSuccess(T t);

    void onFailure(ResponseError responseError);

}
