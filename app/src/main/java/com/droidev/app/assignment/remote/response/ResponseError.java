package com.droidev.app.assignment.remote.response;

public class ResponseError {

    private String error;

    public void setError(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }
}
