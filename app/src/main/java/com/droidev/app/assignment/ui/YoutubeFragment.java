package com.droidev.app.assignment.ui;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.droidev.app.assignment.BuildConfig;
import com.droidev.app.assignment.R;
import com.droidev.app.assignment.contract.YoutubeFragmentContract;
import com.droidev.app.assignment.presenter.MainFragmentPresenter;
import com.droidev.app.assignment.remote.response.ResponseError;
import com.droidev.app.assignment.remote.response.VideoResponse;
import com.droidev.app.assignment.ui.adapter.VideoListAdapter;
import com.droidev.app.assignment.ui.model.ListItemVideo;
import com.droidev.app.assignment.utils.ProgressUtil;
import com.droidev.app.assignment.utils.ToastUtil;
import com.droidev.app.assignment.view.YoutubeView;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.android.youtube.player.YouTubePlayerView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class YoutubeFragment extends ToolbarFragment implements YoutubeView, YouTubePlayer.OnInitializedListener, VideoListAdapter.AdapterListener {

    public static final String TAG = YoutubeFragment.class.getSimpleName();
    private static final int RECOVERY_DIALOG_REQUEST = 1;

    private YoutubeFragmentContract mContract;
    @BindView(R.id.video_list)
    RecyclerView mVideoList;

    private YouTubePlayerFragment mYouTubePlayerFragment;
    private VideoListAdapter mAdapter;
    private YouTubePlayer mYoutubePlayer;

    public YoutubeFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContract = new MainFragmentPresenter(this);
    }

    public static YoutubeFragment newInstance() {
        return new YoutubeFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_youtube, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initToolbar(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mYouTubePlayerFragment = (YouTubePlayerFragment) getActivity().getFragmentManager()
                .findFragmentById(R.id.youtube_player);
        mYouTubePlayerFragment.initialize(BuildConfig.YOUTUBE_KEY, this);
        setListProperties();
    }

    private void setListProperties() {
        mAdapter = new VideoListAdapter(null, this);
        mVideoList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mVideoList.setHasFixedSize(true);
        mVideoList.setAdapter(mAdapter);
    }

    @Override
    public void showProgress() {
        ProgressUtil.show(getActivity(), R.string.loading);
    }

    @Override
    public void hideProgress() {
        ProgressUtil.dismiss();
    }

    @Override
    public void showNoNetworkAlert() {
        showNoNetworkDialog();
    }

    @Override
    public void showResponseError(ResponseError responseError) {
        if (getActivity() != null) {
            ToastUtil.showToast(getActivity(), responseError.getError());
        }
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        mYoutubePlayer = youTubePlayer;
        mContract.getYoutubePlayList();
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (getActivity() != null) {
            if (errorReason.isUserRecoverableError()) {
                errorReason.getErrorDialog(getActivity(), RECOVERY_DIALOG_REQUEST).show();
            } else {
                ToastUtil.showToast(getActivity(), getString(R.string.youtube_error_message));
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            // Retry initialization if user performed a recovery action
            if (getYouTubePlayerProvider() != null) {
                getYouTubePlayerProvider().initialize(BuildConfig.YOUTUBE_KEY, this);
            }
        }
    }

    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        if (getView() != null) {
            return (YouTubePlayerView) getView().findViewById(R.id.youtube_player);
        }
        return null;
    }

    @Override
    public void onYoutubePlayListReceived(VideoResponse videoResponse) {
        if (videoResponse != null) {
            mAdapter.setData(ListItemVideo.getListFrom(videoResponse.getItems()));
            mAdapter.notifyDataSetChanged();
            if (videoResponse.getItems() != null && videoResponse.getItems().size() > 0) {
                mYoutubePlayer.cueVideo(videoResponse.getItems().get(0).getSnippet().getResourceId().getVideoId());
            }
        }
    }

    @Override
    public void onVideoSelected(String videoId) {
        if (mYoutubePlayer != null) {
            mYoutubePlayer.cueVideo(videoId);
            mYoutubePlayer.play();
        }
    }

    @Override
    public void onDestroy() {
        if (mYoutubePlayer != null) {
            mYoutubePlayer.release();
        }
        super.onDestroy();
    }
}
