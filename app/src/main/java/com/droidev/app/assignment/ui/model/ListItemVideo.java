package com.droidev.app.assignment.ui.model;

import com.droidev.app.assignment.remote.response.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shajju on 30/09/2018.
 */

public class ListItemVideo implements ListItem {

    private String mId;
    private String mThumbnail;
    private String mTitle;
    private String mDescription;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getThumbnail() {
        return mThumbnail;
    }

    public void setThumbnail(String thumbnail) {
        mThumbnail = thumbnail;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    @Override
    public int getType() {
        return LIST_ITEM_VIDEO;
    }

    public static ArrayList<ListItem> getListFrom(List<Item> itemList) {
        ArrayList<ListItem> listItems = new ArrayList<>();
        if (itemList != null) {
            for (Item item : itemList) {
                if (item != null) {
                    ListItemVideo itemVideo = new ListItemVideo();
                    if (item.getSnippet() != null) {
                        itemVideo.setDescription(item.getSnippet().getDescription());
                        itemVideo.setTitle(item.getSnippet().getTitle());
                        itemVideo.setId(item.getSnippet().getResourceId().getVideoId());
                        if (item.getSnippet().getThumbnails() != null && item.getSnippet().getThumbnails().getStandard() != null) {
                            itemVideo.setThumbnail(item.getSnippet().getThumbnails().getStandard().getUrl());
                        }
                    }
                    listItems.add(itemVideo);
                }
            }
        }
        return listItems;
    }
}
