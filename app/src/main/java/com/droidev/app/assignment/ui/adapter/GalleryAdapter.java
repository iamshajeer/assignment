package com.droidev.app.assignment.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.droidev.app.assignment.databinding.ListItemGalleryBinding;
import com.droidev.app.assignment.ui.model.ListItem;
import com.droidev.app.assignment.ui.model.ListItemImage;

import java.util.ArrayList;

/**
 * Created by Shajju on 30/09/2018.
 */

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.GalleryHolder> {

    private ArrayList<ListItem> mListItems;

    public GalleryAdapter(ArrayList<ListItem> listItems) {
        mListItems = listItems;
    }

    @NonNull
    @Override
    public GalleryHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case ListItem.LIST_ITEM_IMAGE:
                LayoutInflater layoutInflater =
                        LayoutInflater.from(viewGroup.getContext());
                ListItemGalleryBinding itemGalleryBinding = ListItemGalleryBinding.inflate(layoutInflater, viewGroup, false);
                return new GalleryHolder(itemGalleryBinding);
            default:
                throw new RuntimeException("Unsupported View Type");
        }
    }

    @Override
    public void onBindViewHolder(@NonNull GalleryHolder galleryHolder, int position) {
        ListItem listItem = mListItems.get(position);
        switch (listItem.getType()) {
            case ListItem.LIST_ITEM_IMAGE:
                ListItemImage listItemImage = (ListItemImage) listItem;
                galleryHolder.bind(listItemImage);
                break;
            default:
                //nothing to do
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mListItems == null ? 0 : mListItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mListItems == null ? 0 : mListItems.get(position).getType();
    }

    public void setData(ArrayList<ListItem> listItems) {
        mListItems = listItems;
    }

    class GalleryHolder extends RecyclerView.ViewHolder {

        private final ListItemGalleryBinding mBinding;

        GalleryHolder(@NonNull ListItemGalleryBinding galleryBinding) {
            super(galleryBinding.getRoot());
            this.mBinding = galleryBinding;
        }

        void bind(ListItemImage listItemImage) {
            mBinding.setImage(listItemImage);
        }
    }
}
