package com.droidev.app.assignment.ui;

import android.os.Bundle;

import com.droidev.app.assignment.R;
import com.droidev.app.assignment.base.BaseActivity;
import com.droidev.app.assignment.utils.LocationUtils;
import com.droidev.app.assignment.utils.ToastUtil;

public class MainActivity extends BaseActivity implements HomeFragment.FragmentInteractor {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadHomeScreen();
    }

    private void loadMapFragment() {
        replaceFragment(R.id.fragment_container,
                MapFragment.newInstance(),
                YoutubeFragment.TAG);
    }

    private void loadYoutubeScreen() {
        replaceFragment(R.id.fragment_container,
                YoutubeFragment.newInstance(),
                YoutubeFragment.TAG);
    }

    private void loadGalleryFragment() {
        replaceFragment(R.id.fragment_container,
                GalleryFragment.newInstance(),
                YoutubeFragment.TAG);
    }

    private void loadHomeScreen() {
        addFragment(R.id.fragment_container,
                HomeFragment.newInstance(),
                YoutubeFragment.TAG);
    }

    @Override
    public void onGalleryClicked() {
        loadGalleryFragment();
    }

    @Override
    public void onMapClicked() {
        if (LocationUtils.isLocationEnabled(this)) {
            loadMapFragment();
        } else {
            ToastUtil.showToast(this, getString(R.string.turn_on_location));
        }
    }

    @Override
    public void onYoutubeClicked() {
        loadYoutubeScreen();
    }
}
