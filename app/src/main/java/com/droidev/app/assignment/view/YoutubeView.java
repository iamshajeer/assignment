package com.droidev.app.assignment.view;

import com.droidev.app.assignment.base.BaseView;
import com.droidev.app.assignment.remote.response.VideoResponse;

public interface YoutubeView extends BaseView {

    void onYoutubePlayListReceived(VideoResponse videoResponse);
}
