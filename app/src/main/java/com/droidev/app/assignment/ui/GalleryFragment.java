package com.droidev.app.assignment.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.droidev.app.assignment.R;
import com.droidev.app.assignment.contract.GalleryContract;
import com.droidev.app.assignment.presenter.GalleryPresenter;
import com.droidev.app.assignment.remote.response.ResponseError;
import com.droidev.app.assignment.ui.adapter.GalleryAdapter;
import com.droidev.app.assignment.ui.model.ListItem;
import com.droidev.app.assignment.ui.model.ListItemImage;
import com.droidev.app.assignment.utils.PermissionUtils;
import com.droidev.app.assignment.utils.ProgressUtil;
import com.droidev.app.assignment.utils.ToastUtil;
import com.droidev.app.assignment.view.GalleryView;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Shajju on 30/09/2018.
 */

public class GalleryFragment extends ToolbarFragment implements GalleryView {

    private static final int CAMERA_REQUEST = 100;
    private static final int GALLERY_REQUEST = 101;
    private static final int CAMERA_PERMISSION_REQUEST_CODE = 200;
    private static final int READ_STORAGE_PERMISSION_CODE = 201;
    private static final String TAG = GalleryFragment.class.getSimpleName();

    @BindView(R.id.image_list)
    RecyclerView mImageList;

    private GalleryContract mContract;
    private String mSavedImage;
    private ArrayList<ListItem> mListItems;
    private GalleryAdapter mAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContract = new GalleryPresenter(this);
        mListItems = new ArrayList();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_gallery, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initToolbar(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initGridView();
    }

    private void initGridView() {
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
        mImageList.setLayoutManager(staggeredGridLayoutManager); // set LayoutManager to RecyclerView
        mContract.getAllSavedImages();
    }

    public static GalleryFragment newInstance() {
        return new GalleryFragment();
    }

    @OnClick(R.id.btn_camera)
    public void onCameraClicked() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            PermissionUtils.requestPermission(getActivity(), CAMERA_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else {
            launchCamera();
        }
    }

    private void launchCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File path = getPhotoFileUri();
        Uri fileProvider = Uri.fromFile(path);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileProvider);
        if (cameraIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(cameraIntent, CAMERA_REQUEST);
        }
    }

    public File getPhotoFileUri() {
        String pathname = getActivity().getFilesDir() + "/images/" + new Date().getTime() + ".jpg";
        mSavedImage = pathname;
        File mediaStorageDir = new File(pathname);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
            Log.d(TAG, "failed to create directory");
        }
        // Return the file target for the photo based on filename
        return new File(pathname);
    }

    @OnClick(R.id.btn_gallery)
    public void onGalleryClicked() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            PermissionUtils.requestPermission(getActivity(), READ_STORAGE_PERMISSION_CODE,
                    Manifest.permission.READ_EXTERNAL_STORAGE, true);
        } else {
            openGallery();
        }
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GALLERY_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CAMERA_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    mContract.onImageReceivedFromCamera(mSavedImage);
                }
                break;
            case GALLERY_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    mContract.onImageReceivedFromGallery(data.getData());
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void showProgress() {
        ProgressUtil.show(getActivity(), R.string.loading);
    }

    @Override
    public void hideProgress() {
        ProgressUtil.dismiss();
    }

    @Override
    public void showNoNetworkAlert() {
        showNoNetworkDialog();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case CAMERA_PERMISSION_REQUEST_CODE:
                if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                        Manifest.permission.CAMERA)) {
                    launchCamera();
                } else {
                    ToastUtil.showToast(getActivity(), getString(R.string.permission_required_toast));
                }
                break;
            case READ_STORAGE_PERMISSION_CODE:
                if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    openGallery();
                } else {
                    ToastUtil.showToast(getActivity(), getString(R.string.permission_required_toast));
                }
                break;
        }
    }

    @Override
    public void showResponseError(ResponseError responseError) {

    }

    @Override
    public void onNewImageAdded(String path) {
        ListItemImage listItemImage = new ListItemImage();
        listItemImage.setPath(path);
        mListItems.add(listItemImage);
        mAdapter.setData(mListItems);
        mAdapter.notifyDataSetChanged();
        mImageList.smoothScrollToPosition(mListItems.size());
    }

    @Override
    public void onFilesScanned(ArrayList<ListItem> listItems) {
        mListItems = listItems;
        mAdapter = new GalleryAdapter(listItems);
        mImageList.setAdapter(mAdapter);
    }
}