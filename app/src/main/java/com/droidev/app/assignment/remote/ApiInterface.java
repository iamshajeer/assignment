package com.droidev.app.assignment.remote;


import com.droidev.app.assignment.remote.response.VideoResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("/youtube/v3/playlistItems")
    Call<VideoResponse> getYoutubeFeeds(@Query("key") String developerKey, @Query("playlistId") String playlistId, @Query("part") String id);

}
