package com.droidev.app.assignment.presenter;

import android.content.Context;

import com.droidev.app.assignment.base.AssignmentApplication;
import com.droidev.app.assignment.contract.YoutubeFragmentContract;
import com.droidev.app.assignment.remote.NetworkAdapter;
import com.droidev.app.assignment.remote.ResponseCallback;
import com.droidev.app.assignment.remote.response.ResponseError;
import com.droidev.app.assignment.remote.response.VideoResponse;
import com.droidev.app.assignment.utils.NetworkCheckUtil;
import com.droidev.app.assignment.view.YoutubeView;

import javax.inject.Inject;

public class MainFragmentPresenter implements YoutubeFragmentContract {
    private YoutubeView mView;
    @Inject
    Context mContext;

    public MainFragmentPresenter(YoutubeView youtubeView) {
        AssignmentApplication.getInstance().getAppComponent().inject(this);
        mView = youtubeView;
    }

    @Override
    public void getYoutubePlayList() {
        if (NetworkCheckUtil.isNetworkAvailable(mContext)) {
            mView.showProgress();
            NetworkAdapter.get().getYoutubePlayList(new ResponseCallback<VideoResponse>() {
                @Override
                public void onSuccess(VideoResponse videoResponse) {
                    mView.hideProgress();
                    mView.onYoutubePlayListReceived(videoResponse);
                }

                @Override
                public void onFailure(ResponseError responseError) {
                    mView.hideProgress();
                    mView.showResponseError(responseError);
                }
            });
        } else {
            mView.showNoNetworkAlert();
        }
    }
}
