package com.droidev.app.assignment.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.droidev.app.assignment.databinding.ListItemVideoBinding;
import com.droidev.app.assignment.ui.model.ListItem;
import com.droidev.app.assignment.ui.model.ListItemVideo;

import java.util.List;


public class VideoListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ListItem> mListItems;
    private AdapterListener mListener;

    public VideoListAdapter(List<ListItem> listItems, AdapterListener listener) {
        mListItems = listItems;
        mListener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case ListItem.LIST_ITEM_VIDEO:
                LayoutInflater layoutInflater =
                        LayoutInflater.from(viewGroup.getContext());
                ListItemVideoBinding itemContactBinding = ListItemVideoBinding.inflate(layoutInflater, viewGroup, false);
                return new ContactHolder(itemContactBinding);
            default:
                throw new RuntimeException("Unsupported View Type");
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ListItem listItem = mListItems.get(position);
        switch (listItem.getType()) {
            case ListItem.LIST_ITEM_VIDEO:
                ContactHolder contactHolder = (ContactHolder) viewHolder;
                ListItemVideo listItemVideo = (ListItemVideo) listItem;
                contactHolder.bind(listItemVideo);
                contactHolder.itemView.setOnClickListener(v -> mListener.onVideoSelected(listItemVideo.getId()));
                break;
            default:
                //nothing to do
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mListItems == null ? 0 : mListItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mListItems == null ? 0 : mListItems.get(position).getType();
    }

    public void setData(List<ListItem> data) {
        mListItems = data;
    }

    public class ContactHolder extends RecyclerView.ViewHolder {
        private ListItemVideoBinding mBinding;

        ContactHolder(ListItemVideoBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        void bind(ListItemVideo item) {
            mBinding.setVideo(item);
        }
    }

    public interface AdapterListener {
        void onVideoSelected(String videoId);
    }
}
