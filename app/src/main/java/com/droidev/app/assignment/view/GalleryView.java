package com.droidev.app.assignment.view;

import com.droidev.app.assignment.base.BaseView;
import com.droidev.app.assignment.ui.model.ListItem;

import java.util.ArrayList;

/**
 * Created by Shajju on 30/09/2018.
 */

public interface GalleryView extends BaseView {

    void onNewImageAdded(String path);

    void onFilesScanned(ArrayList<ListItem> mListItems);
}
