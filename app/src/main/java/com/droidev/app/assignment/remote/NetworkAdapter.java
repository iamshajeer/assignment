package com.droidev.app.assignment.remote;

import com.droidev.app.assignment.BuildConfig;
import com.droidev.app.assignment.base.AssignmentApplication;
import com.droidev.app.assignment.remote.response.VideoResponse;

import javax.inject.Inject;

import retrofit2.Call;

public class NetworkAdapter {

    @Inject
    ApiInterface mApiProvider;

    private static NetworkAdapter sNetworkAdapter;

    private NetworkAdapter() {
        AssignmentApplication.getInstance().getAppComponent().inject(this);
    }

    public static NetworkAdapter get() {
        if (sNetworkAdapter == null) {
            sNetworkAdapter = new NetworkAdapter();
        }
        return sNetworkAdapter;
    }

    public void getYoutubePlayList(ResponseCallback<VideoResponse> responseCallback) {
        Call<VideoResponse> call = mApiProvider.getYoutubeFeeds(BuildConfig.YOUTUBE_KEY, BuildConfig.YOUTUBE_PLAY_LIST_ID, "snippet");
        call.enqueue(new RestCallback<>(responseCallback));
    }
}
