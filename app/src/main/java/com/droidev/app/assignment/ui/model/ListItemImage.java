package com.droidev.app.assignment.ui.model;

/**
 * Created by Shajju on 30/09/2018.
 */

public class ListItemImage implements ListItem {

    private String mPath;

    public String getPath() {
        return mPath;
    }

    public void setPath(String path) {
        mPath = path;
    }

    @Override
    public int getType() {
        return ListItem.LIST_ITEM_IMAGE;
    }
}
