package com.droidev.app.assignment.ui;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.droidev.app.assignment.R;
import com.droidev.app.assignment.base.BaseFragment;
import com.droidev.app.assignment.utils.CommonUtils;

public class ToolbarFragment extends BaseFragment {

    protected void initToolbar(View view) {
        if (getActivity() != null) {
            Toolbar toolbar = view.findViewById(R.id.toolbar);
            ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        }
    }

    protected void setToolbarTitle(String title) {
        if (!CommonUtils.isEmpty(title) && getActivity() != null && ((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
        }
    }
}
