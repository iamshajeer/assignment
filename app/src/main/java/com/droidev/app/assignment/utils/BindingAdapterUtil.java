package com.droidev.app.assignment.utils;

import android.databinding.BindingAdapter;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;

public class BindingAdapterUtil {

    /**
     * Binding adapter used to show images in imageView using Picasso library
     *
     * @param imageView imageView to be bounded
     * @param url       image url
     */
    @BindingAdapter({"bind:src"})
    public static void setImageViewResource(ImageView imageView, String url) {
        if (url != null) {
            if (url.startsWith("http") || url.startsWith("https")) {
                Picasso.get()
                        .load(url)
                        .into(imageView);
            } else {
                Picasso.get()
                        .load(new File(url))
                        .into(imageView);
            }
        }
    }

    /**
     * setting text in a text view with the contents trimmed
     *
     * @param textView text view to be used
     * @param text     text to set
     */
    @BindingAdapter({"bind:text"})
    public static void setText(TextView textView, String text) {
        if (!TextUtils.isEmpty(text)) {
            textView.setText(text.trim());
        } else {
            textView.setText("");
        }
    }
}
