package com.droidev.app.assignment.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.droidev.app.assignment.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Shajju on 30/09/2018.
 */

public class HomeFragment extends ToolbarFragment {
    private FragmentInteractor mInteractor;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initToolbar(view);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentInteractor) {
            mInteractor = (FragmentInteractor) context;
        } else {
            throw new RuntimeException("Must extend FragmentInteractor");
        }
    }

    @OnClick(R.id.btn_gallery)
    public void onGalleryClicked() {
        if (mInteractor != null) {
            mInteractor.onGalleryClicked();
        }
    }

    @OnClick(R.id.btn_map)
    public void onMapCliked() {
        if (mInteractor != null) {
            mInteractor.onMapClicked();
        }
    }

    @OnClick(R.id.btn_youtube)
    public void onYoutubeClicked() {
        if (mInteractor != null) {
            mInteractor.onYoutubeClicked();
        }
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    public interface FragmentInteractor {

        void onGalleryClicked();

        void onMapClicked();

        void onYoutubeClicked();
    }
}
