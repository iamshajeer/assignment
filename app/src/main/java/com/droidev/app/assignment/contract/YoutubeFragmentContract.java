package com.droidev.app.assignment.contract;

public interface YoutubeFragmentContract extends BaseContract {

    void getYoutubePlayList();
}
