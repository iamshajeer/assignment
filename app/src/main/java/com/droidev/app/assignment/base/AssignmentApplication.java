package com.droidev.app.assignment.base;

import android.app.Application;

import com.droidev.app.assignment.dagger.DaggerTestAppComponent;
import com.droidev.app.assignment.dagger.TestAppComponent;

public class AssignmentApplication extends Application {

    private static AssignmentApplication mInstance;
    private TestAppComponent mAppComponent;

    public AssignmentApplication() {
        mInstance = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mAppComponent = DaggerTestAppComponent
                .builder()
                .application(this)
                .build();
    }

    public static AssignmentApplication getInstance() {
        return mInstance;
    }

    public TestAppComponent getAppComponent() {
        return mAppComponent;
    }
}
