package com.droidev.app.assignment.dagger.module;

import android.app.Application;
import android.content.Context;

import com.droidev.app.assignment.dagger.TestAppScope;

import dagger.Module;
import dagger.Provides;

@Module
@TestAppScope
public class ApplicationModule {

    @Provides
    Context provideContext(Application application) {
        return application;
    }
}
