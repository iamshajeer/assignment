package com.droidev.app.assignment.presenter;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.webkit.MimeTypeMap;

import com.droidev.app.assignment.base.AssignmentApplication;
import com.droidev.app.assignment.contract.GalleryContract;
import com.droidev.app.assignment.ui.model.ListItem;
import com.droidev.app.assignment.ui.model.ListItemImage;
import com.droidev.app.assignment.view.GalleryView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;

import javax.inject.Inject;

/**
 * Created by Shajju on 30/09/2018.
 */

public class GalleryPresenter implements GalleryContract {
    @Inject
    Context mContext;
    private GalleryView mView;

    public GalleryPresenter(GalleryView galleryView) {
        mView = galleryView;
        AssignmentApplication.getInstance().getAppComponent().inject(this);
    }

    @Override
    public void onImageReceivedFromGallery(Uri selectedImage) {
        InputStream iStream;
        try {
            iStream = mContext.getContentResolver().openInputStream(selectedImage);
            byte[] inputData = getBytes(iStream);
            OutputStream out;
            String root = mContext.getFilesDir() + "/images/";
            File createDir = new File(root + File.separator);
            if (!createDir.exists()) {
                createDir.mkdir();
            }
            File file = new File(root + File.separator + new Date().getTime() + getExtensionOfSelectedImage(selectedImage));
            file.createNewFile();
            out = new FileOutputStream(file);
            out.write(inputData);
            out.close();
            mView.onNewImageAdded(file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getExtensionOfSelectedImage(Uri uri) {
        String extension;
        //Check uri format to avoid null
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            //If scheme is a content
            final MimeTypeMap mime = MimeTypeMap.getSingleton();
            extension = mime.getExtensionFromMimeType(mContext.getContentResolver().getType(uri));
        } else {
            //If scheme is a File
            //This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
            extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());
        }
        return extension;
    }

    private byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    @Override
    public void onImageReceivedFromCamera(String path) {
        mView.onNewImageAdded(path);
    }

    @Override
    public void getAllSavedImages() {
        String path = mContext.getFilesDir() + "/images/";
        ArrayList<ListItem> mListItems = new ArrayList<>();
        File directory = new File(path);
        File[] files = directory.listFiles();
        if (files != null && files.length > 0) {
            for (File file : files) {
                ListItemImage image = new ListItemImage();
                image.setPath(file.getAbsolutePath());
                mListItems.add(image);
            }
        }
        mView.onFilesScanned(mListItems);
    }
}
