package com.droidev.app.assignment.contract;

import android.net.Uri;

/**
 * Created by Shajju on 30/09/2018.
 */

public interface GalleryContract {

    void onImageReceivedFromGallery(Uri data);

    void onImageReceivedFromCamera(String path);

    void getAllSavedImages();
}
