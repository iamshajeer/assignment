package com.droidev.app.assignment.base;

import android.support.v4.app.Fragment;

import com.droidev.app.assignment.R;
import com.droidev.app.assignment.utils.ToastUtil;

public class BaseFragment extends Fragment {

    protected void showNoNetworkDialog() {
        ToastUtil.showToast(getActivity(), getString(R.string.no_network));
    }
}
