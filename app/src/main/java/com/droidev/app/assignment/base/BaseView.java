package com.droidev.app.assignment.base;

import com.droidev.app.assignment.remote.response.ResponseError;

public interface BaseView {

    void showProgress();

    void hideProgress();

    void showNoNetworkAlert();

    void showResponseError(ResponseError responseError);
}
