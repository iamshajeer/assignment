package com.droidev.app.assignment.remote;

import android.support.annotation.NonNull;

import com.droidev.app.assignment.remote.response.ResponseError;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RestCallback<T> implements Callback<T> {

    private ResponseCallback mResponseCallback;

    RestCallback(ResponseCallback responseCallback) {
        mResponseCallback = responseCallback;
    }

    @Override
    public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
        mResponseCallback.onSuccess(response.body());
    }

    @Override
    public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
        mResponseCallback.onFailure(getErrorFrom(t));
    }

    private ResponseError getErrorFrom(Throwable t) {
        ResponseError error = new ResponseError();
        error.setError(t.getMessage());
        return error;
    }
}
