
package com.droidev.app.assignment.remote.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Thumbnails {

    @SerializedName("standard")
    @Expose
    private Standard standard;

    public Standard getStandard() {
        return standard;
    }
}
