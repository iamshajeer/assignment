package com.droidev.app.assignment.dagger;

import android.app.Application;

import com.droidev.app.assignment.dagger.module.ApplicationModule;
import com.droidev.app.assignment.dagger.module.NetworkModule;
import com.droidev.app.assignment.presenter.GalleryPresenter;
import com.droidev.app.assignment.presenter.MainFragmentPresenter;
import com.droidev.app.assignment.remote.NetworkAdapter;

import dagger.BindsInstance;
import dagger.Component;

@Component(modules = {
        NetworkModule.class,
        ApplicationModule.class
})
@TestAppScope
public interface TestAppComponent {

    void inject(NetworkAdapter networkAdapter);

    void inject(MainFragmentPresenter mainFragmentPresenter);

    void inject(GalleryPresenter galleryPresenter);

    @Component.Builder
    interface Builder {
        TestAppComponent build();

        @BindsInstance
        Builder application(Application application);
    }
}
