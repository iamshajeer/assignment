package com.droidev.app.assignment.ui.model;


public interface ListItem {

    int LIST_ITEM_VIDEO = 1;

    int LIST_ITEM_IMAGE = 2;

    int getType();
}
